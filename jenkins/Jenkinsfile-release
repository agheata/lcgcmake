//----------------------------------------------------------------------------------------------------------------------
// This declarative Jenkins pipeline encodes all the steps required for the release of a single platform.
// Other jobs may call this pipelinbe to execute the build, test and installation of a set platforms.
//
// Author: Pere Mato
//----------------------------------------------------------------------------------------------------------------------

//---Global Functions and Maps------(specific to this pileline)---------------------------------------------------------
def lcg
def getTarget() { return params.TARGET != 'all' ? params.TARGET : (params.LCG_VERSION =~ 'LHCB|ATLAS' ? 'top_packages' : 'all') }

pipeline {
  //---Global Parameters------------------------------------------------------------------------------------------------
  parameters {
    string(name:       'LCG_VERSION',          defaultValue: '99', description: 'LCG stack version to build')
    string(name:       'COMPILER',             defaultValue: 'gcc10', description: 'Compiler tag (e.g. native, gcc8, gcc10, clang10, clang11)')
    choice(name:       'BUILDTYPE',            choices: ['Release', 'Debug'])
    string(name:       'LABEL',                defaultValue: 'centos7', description: 'Jenkins label for the OS/Platform or Docker image')
    string(name:       'HOST_ARCH',            defaultValue: 'x86_64', description: 'Jenkins label for the main architecture')
    string(name:       'DOCKER_LABEL',         defaultValue: 'docker-host-noafs', description: 'Label for the the nodes able to launch docker images')
    string(name:       'ARCHITECTURE',         defaultValue: '', description: 'Complement of the architecture (instruction set)')

    string(name:       'LCG_INSTALL_PREFIX',   defaultValue: '', description: 'Location to look for already installed packages matching version and hash value. Leave blank for full rebuilds')
    string(name:       'LCG_ADDITIONAL_REPOS', defaultValue: '', description: 'Additional binary repository')
    string(name:       'LCG_EXTRA_OPTIONS',    defaultValue: '-DLCG_SOURCE_INSTALL=OFF;-DLCG_TARBALL_INSTALL=ON', description: 'Additional configuration options to be added to the cmake command')
    string(name:       'LCG_IGNORE',           defaultValue: '', description: 'Packages already installed in LCG_INSTALL_PREFIX to be ignored (i.e. full rebuild is required). The list is \'single space\' separated.')
    booleanParam(name: 'USE_BINARIES',         defaultValue: true, description: 'Use binaries in repositories')
    string(name:       'TARGET',               defaultValue: 'all', description: 'Target to build (all, <package-name>, ...)')
    choice(name:       'BUILDMODE',            choices: ['release', 'nightly'], description: 'Build mode')
    string(name:       'CVMFS_DEPLOYER',       defaultValue: 'cvmfs-sft', description: 'CVMFS publisher node label')

    booleanParam(name: 'CLEAN_INSTALLDIR',     defaultValue: true, description: 'Instruct to clean the install directory before building')
    booleanParam(name: 'DO_BUILD',             defaultValue: true, description: 'Do the build of the packages (otherwise only configure the stack)')
    booleanParam(name: 'RUN_TESTS',            defaultValue: true, description: 'Run the tests')
    booleanParam(name: 'CVMFS_INSTALL',        defaultValue: false, description: 'Install binaries in CVMFS')
    booleanParam(name: 'VIEWS_CREATION',       defaultValue: true, description: 'View creating while installing in CVMFS')
    booleanParam(name: 'LCGINFO_UPDATE',       defaultValue: true, description: 'Update the LCGinfo')
    booleanParam(name: 'RPM_CREATE',           defaultValue: false, description: 'RPMs creation')
    booleanParam(name: 'RPM_PUBLISH',          defaultValue: false, description: 'RPMs publish')
    choice(name:       'RPM_DRYRUN',           choices: ['no', 'yes'], description: 'RPMs publish dryrun')
    booleanParam(name: 'RPM_TEST',             defaultValue: false, description: 'RPMs tests')
    string(name:       'RPM_REPOSITORY',       defaultValue: '', description: 'RPM repository e.g /eos/project/l/lcg/www/lcgpackages/lcg/repo/7/ or empty')
    string(name:       'RPM_REVISION_NUMBER',  defaultValue: '', description: 'RPM revision number (default is the LCG numeric version)')
  }
  //---Options----------------------------------------------------------------------------------------------------------
  options {
    buildDiscarder(logRotator( daysToKeepStr: '14' ))
    timestamps()
  }
  //---Additional Environment Variables---------------------------------------------------------------------------------
  environment {
    CDASH_TRACK = 'Release'
    CTEST_MODEL = 'Experimental'
    TARGET = getTarget()
    RELEASE_MODE = '1'
    db_pass = credentials('lcgsoft_db_password')
    GITHUB_PAT = credentials('lcgcmake_github_pat')
    PYTHONUNBUFFERED = 'True'
  }

  agent none

  stages {
    //------------------------------------------------------------------------------------------------------------------
    //---Environment Stage to load and set global parameters------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('Environment') {
      agent { label 'startup-jobs' }
      steps {
        script {
          //---Change the build name------------------------------------------------------------------------------------
          def BTYPE = [Release: 'opt', Debug: 'dbg']
          currentBuild.displayName = "#${BUILD_NUMBER}" + ' ' + params.LCG_VERSION + '-' + params.HOST_ARCH + '-' + params.LABEL + '-' + params.COMPILER + '-' + BTYPE[params.BUILDTYPE]
          //---Load common variables and functions between several pilelines--------------------------------------------
          lcg = load 'lcgcmake/jenkins/Jenkinsfunctions.groovy'
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---Build stages---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('InDocker') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'centos|ubuntu|alma|el' }
      }
      agent {
        docker {
          alwaysPull true
          image params.HOST_ARCH =~ "aarch64" ? "gitlab-registry.cern.ch/sft/docker/${LABEL}:aarch64" : "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label params.HOST_ARCH =~ "aarch64" ? "arm64-docker"  : "$DOCKER_LABEL"
          args  """-v /cvmfs:/cvmfs 
                   -v /ccache:/ccache 
                   -v /ec:/ec
                   -v /var/run/nscd:/var/run/nscd
                   -v /eos/project-n:/eos/project-n
                   -e SHELL
                   -e USER
                   -e container=docker
                   -e KRB5CCNAME="FILE:/tmp/krb5cc_14806"
                   --net=host
                   --hostname ${LABEL}-docker
                """
        }
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
        stage('CopyToEOS') {
          steps {
            script {
              lcg.copyToEOS()
            }
          }
        }
        stage ('AbortIfBuildFailed') {
          when {
            beforeAgent true
            expression { currentBuild.result == 'FAILURE' }
          }
          steps { 
          error 'build failure happened on a previous stage, bailing ...'
          }
        }

        stage('CreateRPMs') {
          when {
            expression { params.RPM_CREATE && 
                         params.LABEL =~ 'centos|alma|el' }
          }
          steps {
            script {
              lcg.createRPMS()
            }
          }
        }
      }
    }
    stage('InBareMetal') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'mac' }
      }
      agent {
        label "$LABEL"
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
        stage('CopyToEOS') {
          steps {
            script {
              lcg.copyToEOS()
            }
          }
        }
        stage ('AbortIfBuildFailed') {
          when {
            beforeAgent true
            expression { currentBuild.result == 'FAILURE' }
          }
          steps {
          error 'build failure happened on a previous stage, bailing ...'
          }
        }
        stage('CreateRPMs') {
          when {
            expression { params.RPM_CREATE }
          }
          steps {
            script {
              lcg.createRPMS()
            }
          }
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---CVMFS Installation stage---------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('CVMFSInstall') {
      when {
        beforeAgent true
        expression { params.CVMFS_INSTALL }
      }
      stages {
        stage('MacPreinstall') {
          when {
            beforeAgent true
            expression { params.LABEL =~ 'mac' && params.LABEL != 'mac12arm'  }
          }
          agent {
            label "$LABEL"
          }
          steps {
            script {
              lcg.preinstallMacOS()
            }
          }
        }
        stage('Mac12PreInstall') {
          when {
            beforeAgent true
            expression { params.LABEL == 'mac12arm'  }
          }
          agent {
            docker {
              alwaysPull true
              image "gitlab-registry.cern.ch/sft/docker/centos7"
              label "docker-host-noafs"
              args  """-v /cvmfs:/cvmfs
           -v /ccache:/ccache
           -v /Users/Shared:/Users/Shared
           -v /eos/project-n:/eos/project-n
           -v /ec:/ec
           -v /var/run/nscd:/var/run/nscd
           -e SHELL
           -e USER
           -e container=docker
           -e KRB5CCNAME="FILE:/tmp/krb5cc_14806"
           --net=host
           --hostname ${LABEL}-docker
        """
            }
          }
          steps {
            script {
              lcg.preinstallMacOSOnLinux()
            }
          }
        }
        stage('CVMFSInstall') {
          agent {
            label "$CVMFS_DEPLOYER"
          }
          options {
            lock(resource: "CVMFSInstall-Prod-${CVMFS_DEPLOYER}")
          }
          steps {
            script {
              lcg.installCVMFS()
            }
          }
        }
      }
    }
    stage('LCGinfo') {
      when {
        beforeAgent true
        expression { params.LCGINFO_UPDATE && !(params.LCG_VERSION =~ 'dev') }
      }
      agent {
        label "$DOCKER_LABEL"
      }
      steps {
        lock(resource: "LCGinfo_$params.LCG_VERSION") {
          script {
            lcg.cloneRepository('lcginfo', 'sft')
            if(params.CVMFS_INSTALL) {
              lcg.waitForCVMFS()
            }
            docker.withRegistry( 'https://gitlab-registry.cern.ch', '7c3a9bea-6657-46fc-901d-bef5c7e0061c' ) {
              lcg.publishINFO()
            }
          }
        }
      }
    }
    //----------------------------------------------------------------------------------------------------------------------
    //---Finalize the RPM stages--------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------
    stage('PublishRPMs') {
      when {
        beforeAgent true
        expression { params.RPM_PUBLISH && params.LABEL =~ 'centos|alma|el' }
      }
      agent {
        label 'centos7-physical'
      }
      steps {
        lock('publish_rpms') {
          script {
            lcg.publishRPMS()
          }
        }
      }
    }
    stage('TestRPMs') {
      when {
        beforeAgent true
        expression { params.RPM_TEST && params.LABEL =~ 'centos|alma|el' }
      }
      agent {
          docker {
          alwaysPull true
          image params.HOST_ARCH =~ "aarch64" ? "gitlab-registry.cern.ch/sft/docker/${LABEL}:aarch64" : "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label params.HOST_ARCH =~ "aarch64" ? "arm64-docker"  : "$DOCKER_LABEL"
          args  """-u root
                   -v /var/run/nscd:/var/run/nscd
                   --net=host
                """
        }
 
      }
      steps {
        script {
          lcg.testRPMS()
        }
      }
    }
  }
}

