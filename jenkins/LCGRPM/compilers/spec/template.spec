%define lcgprefix opt/lcg

Name:       {NAME}_{VERSION}_{PLATFORM_SHORT_U}
Version:    3.0.0
Release:    {REVISION}
Summary:    {NAME} {VERSION} {HASH} {PLATFORM_SHORT}
Vendor:     EP-SFT

License:    GPL
Group:      LCG
Source0:    {SOURCE}
Prefix:     /%lcgprefix
AutoReqProv: no
BuildArch:  noarch
Distribution: whatever

Provides: /bin/sh
Provides: {NAME}_{VERSION}_{PLATFORM_SHORT_U}


{DEPS}


%description
{NAME}


%install
mkdir -p %{{?buildroot}}/%lcgprefix 
cd %{{?buildroot}}/%lcgprefix
tar xf %{{_sourcedir}}/{NAME}-{VERSION}_{HASH}-{PLATFORM}.tgz
mv {NAME}/{VERSION} {NAME}/{VERSION}-{HASH}

%post
$RPM_INSTALL_PREFIX/{NAME}/{VERSION}-{HASH}/{PLATFORM_SHORT}/.post-install.sh "/%lcgprefix"
mkdir -p $RPM_INSTALL_PREFIX/{NAME}/{VERSION}
rm -f $RPM_INSTALL_PREFIX/{NAME}/{VERSION}/{PLATFORM_SHORT}
ln -s $RPM_INSTALL_PREFIX/{NAME}/{VERSION}-{HASH}/{PLATFORM_SHORT} $RPM_INSTALL_PREFIX/{NAME}/{VERSION}/{PLATFORM_SHORT}


%files
/%lcgprefix
