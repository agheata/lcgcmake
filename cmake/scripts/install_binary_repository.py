#!/usr/bin/env python3
"""
Program to install a complete set of LCG binary tarfiles.
<pere.mato@cern.ch>
Version 1.0
"""
#-------------------------------------------------------------------------------
import os, sys, tarfile, subprocess, shutil, glob, re, logging
import argparse

FORMAT = '%(asctime)s %(levelname)5s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger("IBR")

if sys.version_info[0] >= 3 :
  import urllib.request as urllib2
else:
  import urllib2

from collections import namedtuple, defaultdict
from install_binary_tarfile import install_tarfile
from buildinfo2json import parse

def tarname_from_buildinfo(buildinfo):
  with open(buildinfo, 'r') as f:
    d = parse(f.read())
  return d['NAME']+'-'+d['VERSION']+'_'+d['HASH']+'-'+d['PLATFORM']+'.tgz'

blacklist = ['ROOT']


def packageVeto(packages):
  """We loop through all packages and blacklist until we didn't change anything during an iteration."""
  packageNames = [p.name for p in packages]
  vetoed = [p.name for p in packages if p.name.split('-')[0] in blacklist and any(x in p.name for x in ['patches','HEAD','master'])]
  oldVetoed = []
  counter = 0
  while oldVetoed != vetoed:
    LOG.info("Iteration %s, we have %s vetoed packages", counter, len(vetoed))
    counter += 1
    oldVetoed = list(vetoed)
    vetoed = [p.name for p in packages if p.name in vetoed or any(d in vetoed for d in p.depends)]
    LOG.info("New vetos: %s", vetoed)
  LOG.info("Done vetoing: we have %s vetoed packages", len(vetoed))
  return vetoed


#---install_repository----------------------------------------------------------
def install_repository(url, prefix, platform, lcgprefix='', maxpack=100):
  #--  ${url}/summary-${platform}.txt
  summary = os.path.join(url, 'summary-'+ platform + '.txt')
  LOG.info("Looking for summary file: %s", summary)
  try:
    resp = urllib2.urlopen(summary)
    tarfiles = resp.read().decode('utf-8').split('\n')
  except urllib2.HTTPError as detail:
    LOG.error('Error downloading %s : %s', summary, detail)
    sys.exit(1)
  except:
    LOG.error('Unexpected error: %s', sys.exc_info()[0])
    sys.exit(1)
  LOG.info("Successfully read summary file")
  LOG.debug("We have %s tarfiles in the summary", len(tarfiles))
  for index, tf in enumerate(tarfiles, start=1):
    LOG.debug("(%s/%s) %s", index, len(tarfiles), tf)

  #---clean purged installations---------------------------------------------
  LOG.info("Cleaning installations")
  LOG.info("Looking for buildinfo: %s", prefix)
  buildinfos = glob.glob(os.path.join(prefix, '*', '*', platform,'.buildinfo_*.txt')) +\
               glob.glob(os.path.join(prefix, 'MCGenerators','*','*', platform,'.buildinfo_*.txt'))
  LOG.info("Found buildinfo: %s", buildinfos)
  for index, info in enumerate(buildinfos, start=1):
    directory = os.path.dirname(info)
    if tarname_from_buildinfo(info) in tarfiles:
      LOG.info('(%s/%s) Keeping %s', index, len(buildinfos), directory)
    elif tarname_from_buildinfo(info) not in tarfiles:
      LOG.info('(%s/%s) Deleting %s', index, len(buildinfos), directory)
      shutil.rmtree(directory, ignore_errors=True)
      #---Prune empty directories-----------------------------------------------
      p = os.path.dirname(directory)
      while p != prefix :
          if not os.listdir(p):
              LOG.info('Removing empty directory: %s', p)
              os.rmdir(p)
          elif os.listdir(p) == [".cvmfscatalog"]:
            LOG.info('Removing catalog only directory: %s', p)
            shutil.rmtree(p, ignore_errors=True)
          p = os.path.dirname(p)
  npackages = 0
  ipackages = 0
  package = namedtuple('package', ['tarfile', 'directory', 'name', 'depends'])
  packages = []

  #---Loop over all tarfiles----------------------------------------------------
  LOG.info("Looping over all %s tarfiles", len(tarfiles))
  for index, file in enumerate(tarfiles, start=1):
    if (file == '' or 'summary' in file
        or file.startswith('.sys.a#.v')):  # filter eos artifacts
      LOG.info("(%s/%s) Skipping summary or artifact: %s", index, len(tarfiles), file)
      continue
    #---Obtain the hash value
    try: 
      urltarfile = os.path.join(url, file)
      hash =  os.path.splitext(file)[0].split('-')[-5].split('_')[-1]
    except:
      LOG.info("(%s/%s) Binary tarfile name %r ill-formed", index, len(tarfiles), file)
      continue
    #---Open the remote tarfiles to get the true dirname and version------------
    try:
      LOG.info("(%s/%s) Opening tarfile %s", index, len(tarfiles), urltarfile)
      resp = urllib2.urlopen(urltarfile)
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      dirname, version = os.path.split(tar.next().name)
    except urllib2.HTTPError as detail:
      LOG.error('Error accessing %s : %s', urltarfile, detail)
      continue
    except tarfile.ReadError as detail:
      LOG.error('Error untaring %s : %s', urltarfile, detail)
      continue
    except:
      LOG.error('Unexpected error: %s', sys.exc_info()[0])
      sys.exit(1)

    targetdir = os.path.join(prefix, dirname, version + '-' + hash, platform)
    if not os.path.exists(targetdir):
      #  Get dependencies information from tarfile to be installed
      resp = urllib2.urlopen(urltarfile)
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      for member in tar:
        if member.isfile() and re.search('.buildinfo_.*\.txt', member.name):
          buildinfo = tar.extractfile(member).read().decode('utf-8')
          break
      p = parse(buildinfo)
      packages.append(package(tarfile=urltarfile, \
                              directory=targetdir,\
                              name=p['NAME']+'-'+p['VERSION']+'-'+p['HASH'],\
                              depends=p['DEPENDS']))
      npackages += 1

  LOG.info("Vetoing packages")
  vetoed = packageVeto(packages)

  #---loop over the sorted tarfiles---------------------------------------------
  LOG.info("Installing packages now")
  for index, package in enumerate(packages, start=1):
    if package.name in vetoed:
      LOG.info("(%s/%s) Skipping package: %s", index, len(packages), package.name)
      continue
    LOG.info("(%s/%s) Installing package: %s: url=%s", index, len(packages), package.name, package.tarfile)
    install_tarfile(urltarfile=package.tarfile, prefix=prefix, lcgprefix=lcgprefix, with_hash=True, with_link=False)
    ipackages += 1

    if ipackages >= maxpack : break

  LOG.info('Installed %d packages of a total of %d packages', ipackages, npackages)

#---Main program----------------------------------------------------------------
if __name__ == '__main__':

  #---Parse the arguments---------------------------------------------------------
  parser = argparse.ArgumentParser()
  parser.add_argument('--url', dest='url', help='URL of the binary cache repository', required=True)
  parser.add_argument('--prefix', dest='prefix', help='prefix to the installation', required=True)
  parser.add_argument('--lcgprefix', dest='lcgprefix', help='LCG prefix to the installation', default='', required=False)
  parser.add_argument('--platform', dest='platform', help='select a given platform', required=True)
  parser.add_argument('--maxpack', dest='maxpack', type=int, help='maximum number of packages to be installed', default=100, required=False)
  
  args = parser.parse_args()
  LOG.debug("Parsed parameters")
  LOG.debug(f"Installing repository: url=%s, prefix=%s, platform=%s, lcgprefix=%s, maxpack=%s", args.url, args.prefix, args.platform, args.lcgprefix, args.maxpack)
  install_repository(args.url, args.prefix, args.platform, args.lcgprefix, args.maxpack)
