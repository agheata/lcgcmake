#---List of externals--------------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Fix the versions for the most relevant packages -------------------
LCG_external_package(ROOT              6.26.00                        )
LCG_external_package(veccore           0.8.0                          )
LCG_external_package(VecGeom           1.1.19                         )
LCG_external_package(clhep             2.4.5.1                        )
LCG_external_package(pybind11          2.7.1                          )
LCG_external_package(hdf5              1.12.1                         )

#---Define the top level packages for this stack-----------------------
LCG_top_packages(ROOT VecGeom Python Qt5 tbb XercesC clhep expat ninja
                 ccache CMake lcgenv hdf5 valgrind pybind11)
