#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT v6-28-00-patches GIT=http://root.cern.ch/git/root.git)
#LCG_external_package(ROOT 6.28.00)

#if(${LCG_OS}${LCG_OSVERS} MATCHES centos|ubuntu|el)
#  if(((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} GREATER 7)) OR (${LCG_COMP} MATCHES clang))
#    LCG_AA_project(Gaudi  v36r14.testtbb GIT=https://gitlab.cern.ch/dkonst/Gaudi.git)
#  endif()
#endif()

# Downgrade to these versions of spark and hadoop to be put into LCG_104a
if((${LCG_OS} MATCHES centos|el|ubuntu) AND (NOT ${LCG_COMP} STREQUAL clang))
  LCG_external_package(spark           3.4.1                                    )
  LCG_external_package(hadoop          3.3.4                                    )
endif()

#---Apple MacOS special removals and overwrites--------------------
include(heptools-macos)

if(LCG_ARCH MATCHES "aarch64")
  include(heptools-devARM)
endif()

