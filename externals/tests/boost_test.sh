#!/bin/sh -e

# $2: Python version with two digits

# look for Boost compiled modules in $1
mods=$(ls $1/ | sed -nE 's/^libboost_([a-z0-9_]*)(-.*)?\.(dylib|so)$/\1/p')

python_module=""
if [[ "$2" != "" ]] ; then
  python_version=`echo $2|sed -e 's/\.//'`
  python_module="python${python_version}"
fi

# `signals` module is gone since 1.68
expected_mods="atomic chrono container context coroutine date_time filesystem graph iostreams locale log log_setup prg_exec_monitor program_options $python_module random regex serialization system thread timer unit_test_framework wave wserialization"

# check that none of the expected modules is missing
missing_mods=$( (echo $mods ; echo $mods ; echo $expected_mods) | tr ' ' '\n' | sort | uniq -u )
if [ -n "$missing_mods" ] ; then
  echo error: missing modules: $missing_mods
  exit 1
fi
